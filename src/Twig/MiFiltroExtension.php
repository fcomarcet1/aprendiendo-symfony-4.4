<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class MiFiltroExtension extends AbstractExtension {

	public function getFilters(): array {
		return [
			// If your filter generates SAFE HTML, you should add a third
			// parameter: ['is_safe' => ['html']]
			// Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
			new TwigFilter('tablaMultiplicar', [$this, 'tablaMultiplicar']),
		];
	}

	public function getFunctions(): array {
		return [
			new TwigFunction('tablaMultiplicar', [$this, 'tablaMultiplicar']),
		];
	}

	public function tablaMultiplicar($numero) {
		
		$tabla = "<h3>Tabla del numero: $numero</h3>";
		for($i = 0; $i <= 10; $i++) {
			$tabla .= "<p>$numero X $i = " .($numero*$i)."</p>";
		}
		
		return $tabla;
	}

}
