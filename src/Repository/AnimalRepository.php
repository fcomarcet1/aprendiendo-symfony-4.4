<?php

namespace App\Repository;

use App\Entity\Animal;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class AnimalRepository extends ServiceEntityRepository
{
	// contructor
	public function __construct(ManagerRegistry $registry) {
		parent::__construct($registry, Animal::class);
	}
	
	// Han de empezar los metodos por findBy...
	public function findByAnimalsOrder($order) {
		
		$query_builder = $this->createQueryBuilder('a')
				//->setParameter($parameter)
				//->andWhere($condition)
				->orderBy('a.id', 'DESC')
				->getQuery();
		
		$resultQuery = $query_builder->execute();
	
		//return $resultQuery;
		
		//Podriamos devolver lo que queramos en vez del resultQuery por ej un array
		$collection_output = array (
			'name' => 'Listado animales ordenados por id DESC',
			'animals' => $resultQuery,
		);
		
		return $collection_output;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}
