<?php

namespace App\Form;

//Librerias form en clases
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class AnimalType extends AbstractType {

	// se tiene que llamar obligatoriamente buildForm
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder->add("tipo", TextType::class, [
					'label' => 'Tipo de animal', // cambiar label del campo
						//'help' => 'Inserte un tipo de animal',
				])
				->add("raza", TextType::class)
				->add("color", TextType::class)
				->add("submit", SubmitType::class, [
					'label' => 'Enviar', // Value
					'attr' => ['class' => 'btn'],
		]);
	}

}
