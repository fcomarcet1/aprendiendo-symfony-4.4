<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


class HomeController extends AbstractController
{

    /**
     * @Route("/home", name="home")
     */
    public function index()
    {
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'var_devuelta' => 'Hola mundo',
        ]);

    }

    public function animales($nombre, $apellidos, $edad)
    {

        //var_dump($nombre); die();
        $title = "Bienvenido a la pagina de Animales";
        $animales = array("perro", "pangolin", "murcielago", "gato");
        $pajaro = [
            'raza' => 'paloma',
            'sexo' => 'Macho',
            'zona' => 'Mediterraneo',
        ];

        return $this->render('home/animales.html.twig', [
            'titulo' => $title,
            'nombre' => $nombre,
            'apellidos' => $apellidos,
            'edad' => $edad,
            'animales' => $animales,
            'pajaro' => $pajaro,

        ]);

    }

    public function redirigir()
    {
        //        redirigir a una ruta
        //        return $this->redirectToRoute('index');

        //        redireccion con codigo 301
        //        return $this->redirectToRoute('index', array() ,301);

        //        paso de parametros en la redireccion
        //        return $this->redirectToRoute('animales', [
        //            'nombre' => 'manolo',
        //            'apellidos' => 'Sanchez',
        //            'edad' => 55
        //        ]);

        //redireccion a una URL
        return $this->redirect("https://www.24h-lemans.com/en");

    }

}
