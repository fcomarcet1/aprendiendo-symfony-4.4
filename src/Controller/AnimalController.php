<?php

namespace App\Controller;

use App\Entity\Animal; //* Cargamos clase Animal
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

//forms
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

use App\Form\AnimalType;

// Validacion
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints\Email;

class AnimalController extends AbstractController {

	public function index(): Response {

		//* cargar repositorio de metodos doctrine
		$doctrine = $this->getDoctrine();
		$animal_repository = $doctrine->getRepository(Animal::class);

		//* Obtener todos los animales
		$animales = $animal_repository->findAll();

		//* Obtener animal en base a una condicion.
		$animal = $animal_repository->findOneBy([
			'tipo' => "Murcielago Xinoso",
			'color' => "blanco",
		]);

		//* Obtener animales en base a una condicion + ORDER BY id DESC.
		$animals = $animal_repository->findBy(array(
			'raza' => "Mamífero",
				), array(
			'id' => "DESC",
		));

		//var_dump($animal); die();

		return $this->render('animal/index.html.twig', [
					'controller_name' => 'AnimalController',
					'animales' => $animales,
					'animal' => $animal,
					'animals' => $animals,
		]);
	}
	
	
	public function validarEmail($email)  {
		//var_dump($email); die();
		$validator = Validation::createValidator();
		$errores = $validator->validate($email, [
			new Email()
		]);
		
		if(count($errores) != 0){
			$message = " El email no se ha validado";
			
			foreach($errores as $error){
				echo $error->getMessage()."<br/>";
			}
			
		}else{
			$message =  " El email si se ha validado";
		}
		
		return new Response($message);
	}

	// Form crear nuevo registro
	public function createAnimalForm(Request $request) {
		
		//* Creamos nuevo obj
		$animal = new Animal();
		//* Generamos el formulario usando la clase AnimalType
		$form = $this->createForm(AnimalType::class, $animal);	
		//* Recoger datos del formulario y los almacena
		//*  en el obj en este caso $animal
		$form->handleRequest($request);

		// Comprobamos si se ha enviado el form
		if ($form->isSubmitted() && $form->isValid()) {
			//var_dump($animal); die();
			//* Almacenar datos en la BD
			$doctrine = $this->getDoctrine();
			$entityManager = $doctrine->getManager();
			$entityManager->persist($animal);
			$entityManager->flush();
			
			//* SESSION FLASH
			$session = new Session();
			//$session->start(); // No es necesario
			$session->getFlashBag()->add('message', 'Animal creado correctamente');
			return $this->redirectToRoute("crear_animal");
		}
		
		//* Renderizamos la vista
		return $this->render("animal/add_animal.html.twig", [
					'form' => $form->createView(),
		]);
	}

	// Almacenar datos en BD
	public function save() {

		/*
		  EJEMPLO
		  //* GUARDAR EN TABLA DE BASE UNA DE DATOS
		  //* Cargar EntityManager
		  // Usamos los metodos para poder trabajar con las entidades y poder guardarlas en la BD
		  $entityManager = $this->getDoctrine()->getManager();

		  // Creamos objeto y setear valores
		  $animal = new Animal();
		  $animal->setTipo("Murcielago Xinoso");
		  $animal->setColor("Negro");
		  $animal->setRaza("Mamífero");

		  //* Persistir objetos(guardar) en doctrine
		  $entityManager->persist($animal);
		  //* Volcado de datos en la tabla
		  $entityManager->flush();

		  return new Response("El animal almacenado tiene el ID:" . $animal->getId());

		 */
	}

	//*Obtener animal por id.
	public function getAnimal(int $id) {
		//* cargar repositorio de metodos doctrine
		$animal_repository = $this->getDoctrine()->getRepository(Animal::class);
		//* Consulta
		$animal = $animal_repository->find($id);

		if (!$animal) {
			$message = "El animal no existe";
		} else {
			$message = "El animal seleccionado es " . $animal->getTipo() . "-" . $animal->getRaza();
		}

		return new Response($message);
	}

	//* Obtener objeto automaticamenta sin usar find()
	public function getAnimalAuto(Animal $animalAuto) {

		if (!$animalAuto) {
			$message = "El animal no existe";
		} else {
			$message = "El animal seleccionado es " . $animalAuto->getTipo() . "-" . $animalAuto->getRaza();
		}

		return new Response($message);
	}

	//* Actualizar registro
	public function update($id) {

		// Cargar doctrine para usar ORM.
		$doctrine = $this->getDoctrine();

		// Cargar EntityManager(Nos permite guardar/actualizar/borrar ...etc en la BD), por
		// NO necesario si solo vamos a realizar consultas
		$entityManager = $doctrine->getManager();

		// Cargar repositorio Animal para efectuar la consulta
		$className = Animal::class;
		$animalRepository = $entityManager->getRepository($className);

		// Consulta find(), para obtener el objeto a actualizar
		$animal = $animalRepository->find($id);

		// Comprobar si el objeto llega
		if (!$animal) {
			$message = "El animal no existe en la BD";
		} else {
			// setear valores al objeto
			$animal->setTipo("Perro$id");
			$animal->setColor("rojo");

			// Persistir en doctrine(no es necesario en update)
			$entityManager->persist($animal);

			// Guardar en la BD-> flush()
			$entityManager->flush();

			$message = "Has actualizado el animal " . $animal->getId();
		}
		// Devolver response
		return new Response($message);
	}

	public function updateAuto(Animal $animal = null) {

		if (($animal && is_object($animal)) && (!empty($animal) && $animal != null)) {
			// Cargar doctrine y entityManager
			$doctrine = $this->getDoctrine();
			$entityManager = $doctrine->getManager();

			$animal->setTipo("Murcielago latinoamericanp");
			$animal->setColor("rosa");

			$entityManager->persist($animal);
			$entityManager->flush();

			$message = "El registro se actualizó correctamente";
		} else {
			$message = 'ERROR.No existe el animal a borrar';
		}

		return new Response($message);
	}

	//* Eliminar registros
	public function delete(Animal $animal = null) {

		if (($animal && is_object($animal)) && (!empty($animal) || $animal != null)) {
			//var_dump($animal); die();
			// Cargar doctrine y entityManager
			$doctrine = $this->getDoctrine();
			$entityManager = $doctrine->getManager();

			// eliminar registro memoria doctrine
			$entityManager->remove($animal);

			// eliminar de la BD
			$entityManager->flush();

			$message = "El registro se elimino correctamente";
		} else {
			$message = 'ERROR.No existe el animal a borrar';
		}

		//$message = "Borrado";
		return new Response($message);
	}

	// Uso del Query builder
	public function queryBuilder(): Response {

		// Cargar doctrine y repositorio
		$doctrine = $this->getDoctrine();
		$animal_repository = $doctrine->getRepository(Animal::class);

		// Consulta
		$condition = "a.raza = 'Mamífero' ";
		$parameter = "a.raza = :raza";

		// createQuerybuilder('alias')
		$query_builder = $animal_repository->createQueryBuilder('a')
				//->setParameter($parameter)
				//->andWhere($condition)
				->orderBy('a.id', 'DESC')
				->getQuery();

		// Ejecutamos la consulta
		$resultQuery = $query_builder->execute();

		return $this->render('animal/query_builder.html.twig', [
					'controller_name' => 'AnimalController',
					'animals' => $resultQuery,
		]);
	}

	// Uso del DQL
	public function DQL_Languaje() {

		// Cargar doctrine y repositorio
		$doctrine = $this->getDoctrine();
		$entityManager = $doctrine->getManager();
		//$animal_repository = $doctrine->getRepository(Animal::class);
		// CONSULTA
		// SELECT * FROM animales WHERE raza = "Mamífero"
		$dql = " SELECT a FROM App\Entity\Animal a WHERE a.raza = 'Mamífero' ";
		$dql2 = " SELECT a FROM App\Entity\Animal a ORDER BY a.id DESC ";
		$query_dql = $entityManager->createQuery($dql2);

		// Ejecutamos la consulta
		$resultSet = $query_dql->execute();

		return $this->render('animal/dql_languaje.html.twig', [
					'controller_name' => 'AnimalController',
					'animales' => $resultSet,
		]);

		// Uso de SQL
	}

	// Uso de SQL
	public function SQL_Languaje() {
		// Cargamos doctrine
		$doctrine = $this->getDoctrine();

		// Establecemos conexion con la BD
		$connection = $doctrine->getConnection();

		// Sentencia SQL
		$sql = "SELECT * FROM animales ORDER BY id DESC";

		// Preparamos consulta
		$prepareQuery = $connection->prepare($sql);

		// Ejecutamos consulta // devuelve true o false
		$execute = $prepareQuery->execute();

		// Realizamos un fetchAll() para obtener los valores
		$resultSet = $prepareQuery->fetchAll();

		//var_dump($resultSet);die();

		return $this->render('animal/sql_languaje.html.twig', [
					'controller_name' => 'AnimalController',
					'animales' => $resultSet,
		]);
	}

	// Uso de un repositorio
	public function Use_Repository() {

		// Cargar doctrine y repositorio
		$doctrine = $this->getDoctrine();
		$animal_repository = $doctrine->getRepository(Animal::class);

		// Llamamos al metodo del repositorio findById
		$animales = $animal_repository->findByAnimalsOrder("DESC");

		//var_dump($animales);

		return $this->render('animal/repository.html.twig', [
					'controller_name' => 'AnimalController',
					'animals' => $animales,
		]);
	}

}
